package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spellList;
    public ChainSpell(ArrayList spellList){
        this.spellList = spellList;
    }

    @Override
    public void cast(){
        for(Spell sp : spellList){
            sp.cast();
        }
    }

    @Override
    public void undo(){
        for(int i = this.spellList.size()-1; i >= 0; i--){
            this.spellList.get(i).undo();
        }
    }

    @Override
    public String spellName(){
        return "ChainSpell";
    }
}
