package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
        }

        //ToDo: Complete Me
        public void update(){
                String otherQuest = guild.getQuestType();
                if(otherQuest.equals("D") || otherQuest.equals("E")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
