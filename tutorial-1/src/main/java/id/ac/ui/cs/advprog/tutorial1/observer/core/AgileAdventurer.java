package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
        }

        //ToDo: Complete Me
        public void update(){
                String otherQuest = guild.getQuestType();
                if(otherQuest.equals("D") || otherQuest.equals("R")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
